// OPEN AND CLOSE DASHBOARD
var dashboard = document.querySelector(".dashboard");
var mainContent = document.querySelector(".main-content");
var openDash = document.querySelector(".nav-left-menu");
var closeDash = document.querySelector(".dashboard-close-menu");

openDash.addEventListener("click", () => {
  dashboard.style.display = "block";
  mainContent.classList.remove("toggle");
});
closeDash.addEventListener("click", () => {
  dashboard.style.display = "none";

  mainContent.classList.add("toggle");
});
